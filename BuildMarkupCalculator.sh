#!/bin/bash

#This very simplistic script compiles the library and tests without using Maven/Ant.

echo "*** Cleaning previous build ***"

rm build/*.jar
rm build/markupcalculator/*.class
rm test/markupcalculator/*.class

set -e

echo "*** Compiling Main Source Files ***"

javac -sourcepath src -d build src/markupcalculator/*.java

echo "*** Compiling Tests ***"

javac -verbose -cp external/junit-4.12.jar:build test/markupcalculator/*.java

echo "*** Running Tests ***"

java -cp test/markupcalculator:build/:external/* MarkupCalculatorTestRunner

#Redundant for this assignment but library should be fully usable.
echo "*** Generating JAR file for library ***"

cd build
jar cfv MarkupCalculator.jar markupcalculator/*.class
cd ..