package markupcalculator;

import markupcalculator.*;
import markupcalculator.MarkupConstants.*;

public class MarkupCalculator{
	//All functions are statically accessed so prevent any constructors both within the class and outside.
	private MarkupCalculator(){
	    throw new AssertionError();
	}

	//Wrapper that accepts material name and converts it to a MaterialRate using the hard coded dictionary.
	public static double calculatePrice(double basePrice, int numPersons, String materialName){
		if(!MarkupConstants.MaterialDictionary.containsKey(materialName.toLowerCase())){
			throw new MarkupCalculatorException(MarkupCalculatorException.ExceptionCode.INVALID_MATERIAL);
		}

		MaterialRates rate =  MarkupConstants.MaterialDictionary.get(materialName.toLowerCase());
		return calculatePrice(basePrice, numPersons, rate);
	}

	public static double calculatePrice(double basePrice, int numPersons, MaterialRates rate){
		if(basePrice <= 0){
			throw new MarkupCalculatorException(MarkupCalculatorException.ExceptionCode.INVALID_BASE_PRICE);
		}

		if(!Persons.isWithinLimits(numPersons)){
			throw new MarkupCalculatorException(MarkupCalculatorException.ExceptionCode.INVALID_NUMBER_OF_PERSONS);
		}

		double flatMarkup = calculateFlatMarkup(basePrice);
		double personsMarkup = calculatePersonsMarkup(basePrice + flatMarkup, numPersons);
		double materialsMarkup = calculateMaterialsMarkup(basePrice + flatMarkup, rate);

		return basePrice + flatMarkup + personsMarkup + materialsMarkup;
	}

	public static double calculateFlatMarkup(double basePrice){
		return  basePrice * BasicRates.FLAT.getFraction();
	}

	public static double calculatePersonsMarkup(double basePrice, int numPersons){
		return  basePrice * numPersons * BasicRates.PERSON.getFraction();
	}

	public static double calculateMaterialsMarkup(double basePrice, MaterialRates rate){
		return basePrice * rate.getFraction();
	}
}