package markupcalculator;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

public final class MarkupConstants{

	//All values are statically accessed so prevent any constructors both within the class and outside.
	private MarkupConstants(){
	    throw new AssertionError();
	}

	// Store a local dictionary to convert material aliases to keys used to find rates. Can also be a database.
	public static final Map<String, MaterialRates> MaterialDictionary
	    = Collections.unmodifiableMap(new HashMap<String, MaterialRates>() {{ 
       	put("pharmaceuticals", MaterialRates.PHARMACEUTICALS);
		put("drugs", MaterialRates.PHARMACEUTICALS);
		put("food", MaterialRates.FOOD);
		put("electronics", MaterialRates.ELECTRONICS);
		put("cellphones", MaterialRates.ELECTRONICS);
		put("books", MaterialRates.OTHER);
    }});

	// Mark-up Rates in %. Change with changing business requirements. Can also be an XML.
	public static enum BasicRates{
		FLAT (5.0f),
		PERSON (1.2f);

		private final double value;
	   	BasicRates(double value) { 
	       this.value = value;
	   	}

	   	public double getFraction(){
	   		return value/100;
	   	}
	}

	// Mark-up Rates in %. Change with changing business requirements. Can also be an XML file.
	public static enum MaterialRates{
		PHARMACEUTICALS (7.5f),
		FOOD (13.0f),
		ELECTRONICS (2.0f),
		OTHER (0f);

		private final double value;
	   	MaterialRates(double value) { 
	       this.value = value;
	   	}

	   	public double getFraction(){
	   		return value/100;
	   	}

	}

	// Assuming there is an upper bound for the maximum number of people who work on a project. Can be used to reject unreasonable values.
	public static enum Persons{
		MIN (1),
		MAX (1000);

		public final int value;
	   	Persons(int value) { 
	       this.value = value;
	   	}

		public static Boolean isWithinLimits(int numPersons){
			if(numPersons < MIN.value 
			|| numPersons >= MAX.value){
				return false;
			}
			return true;
		}
	}
}
