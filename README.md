# Nulogy Assignment #

### Environment ###

Operating System: 32-bit Lubuntu 14.10 VM
Java Version: 1.7.0_75
OpenJDK Runtime Environment (IcedTea 2.5.4) (7u75-2.5.4-1~utopic1)
OpenJDK Client VM (build 24.75-b04, mixed mode, sharing)

### Compiling The Code ###

Note: All the binaries should already be in the repository so it is not necessary to compile.

To recompile, run "./BuildMarkupCalculator.sh" at the root of the directory. The script will clean all the binaries, generate new class files, run the tests and create a jar file.

### Running the tests ###

The test runner binary can be invoked by running "./RunTests.sh" at the root of the directory.

### Comments ###

The jar file is redundant but can still be used as library. The build script was created so that everything can be run from the CLI without depending on any specific build system or IDE.
