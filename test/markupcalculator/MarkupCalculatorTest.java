import org.junit.* ;
import static org.junit.Assert.*;
import markupcalculator.*;

//Define test cases
public class MarkupCalculatorTest{
	final double tolerance = 0.01;

	/* Unit testing */
	@Test
	public void test_calculateFlatMarkup(){
		System.out.println("Unit Test the calculateFlatMarkup() helper, basePrice: 100, output: 5");
		double result = MarkupCalculator.calculateFlatMarkup(100);
		assertEquals(5, result, tolerance);
	}

	@Test
	public void test_calculatePersonsMarkup(){
		System.out.println("Unit Test the calculatePersonsMarkup() helper, basePrice: 100, numPersons: 2, output: 2.4");
		double result = MarkupCalculator.calculatePersonsMarkup(100, 2);
		assertEquals(2.4, result, tolerance);
	}

	@Test
	public void test_calculateMaterialsMarkup(){
		System.out.println("Unit Test the calculateMaterialsMarkup() helper, basePrice: 100, rate: PHARMACEUTICALS, output: 2.4");
		double result = MarkupCalculator.calculateMaterialsMarkup(100, MarkupConstants.MaterialRates.PHARMACEUTICALS);
		assertEquals(7.5, result, tolerance);
	}

	/* Scenario Testing */

	//Scenario 1: $1299.99 3 people, food, Result is $1591.58
	@Test
	public void test_calculatePriceWithScenario1(){
		System.out.println("Scenario 1 with $1299.99 3 people, food, Result should be $1591.58");
		double result = MarkupCalculator.calculatePrice(1299.99, 3, "food");
		assertEquals(1591.58, result, tolerance);
	}

	//Scenario 2: $5432.00, 1 person, drug, Result is $6199.81
	@Test
	public void test_calculatePriceWithScenario2(){
		System.out.println("Scenario 2 with $5432.00, 1 person, drug, Result should be $6199.81");
		double result = MarkupCalculator.calculatePrice(5432, 1, "drugs");
		assertEquals(6199.81, result, tolerance);
	}

	//Scenario 3: $12456.95, 4 people, books, Result is $13707.63
	@Test
	public void test_calculatePriceWithScenario3(){
		System.out.println("Scenario 3 with $12456.95, 4 people, books, Result should be $13707.63");
		double result = MarkupCalculator.calculatePrice(12456.95, 4, "books");
		assertEquals(13707.63, result, tolerance);
	}

	/* Edge Case Testing */

	//Test with negative number of persons
	@Test
	public void test_negativeNumberOfPersonsException(){
		System.out.println("Edge Case Test with negative number of persons, Result: exception should be thrown and caught");
		try{
			double result = MarkupCalculator.calculatePrice(12456.95, -4, "books");
			fail();
		}catch(MarkupCalculatorException e){
			if(e.getCode() != MarkupCalculatorException.ExceptionCode.INVALID_NUMBER_OF_PERSONS){
				fail("Incorrect Exception Code was returned");
			}
		}
	}

	//Test with really high number of persons
	@Test
	public void test_highNumberOfPersonsException(){
		System.out.println("Edge Case Test with really high number of persons, Result: exception should be thrown and caught");
		try{
			double result = MarkupCalculator.calculatePrice(12456.95, 1000000, "books");
			fail();
		}catch(MarkupCalculatorException e){
			if(e.getCode() != MarkupCalculatorException.ExceptionCode.INVALID_NUMBER_OF_PERSONS){
				fail("Incorrect Exception Code was returned");
			}
		}
	}

	//Test with unknown material type
	@Test
	public void test_unknownMaterialTypeException(){
		System.out.println("Edge Case Test with really high number of persons, Result: exception should be thrown and caught");
		try{
			double result = MarkupCalculator.calculatePrice(12456.95, 10, "I'M NOT VALID :(");
			fail();
		}catch(MarkupCalculatorException e){
			if(e.getCode() != MarkupCalculatorException.ExceptionCode.INVALID_MATERIAL){
				fail("Incorrect Exception Code was returned");
			}
		}
	}

	//Test with camel case material type (cellphones will be CellPhones)
	@Test
	public void test_noExceptionWithCamelCaseMaterialInput(){
		System.out.println("Edge Case Test with camel case material type, Result: no exception should be thrown");
		double result = MarkupCalculator.calculatePrice(12456.95, 10, "CellPhones");
	}

	//Test with negative base price
	@Test
	public void test_negtiveBasePriceException(){
		System.out.println("Edge Case Test with negative base price, Result: exception should be thrown and caught");
		try{
			double result = MarkupCalculator.calculatePrice(-12456.95, 10, "electronics");
			fail();
		}catch(MarkupCalculatorException e){
			if(e.getCode() != MarkupCalculatorException.ExceptionCode.INVALID_BASE_PRICE){
				fail("Incorrect Exception Code was returned");
			}
		}
	}

	//Test with zero base price
	@Test
	public void test_zeroBasePriceException(){
		System.out.println("Edge Case Test with zero base price, Result: exception should be thrown and caught");
		try{
			double result = MarkupCalculator.calculatePrice(0, 10, "electronics");
			fail();
		}catch(MarkupCalculatorException e){
			if(e.getCode() != MarkupCalculatorException.ExceptionCode.INVALID_BASE_PRICE){
				fail("Incorrect Exception Code was returned");
			}
		}
	}

}
