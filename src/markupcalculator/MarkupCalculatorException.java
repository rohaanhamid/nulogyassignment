package markupcalculator;

import markupcalculator.*;
import markupcalculator.MarkupConstants.*;

public class MarkupCalculatorException extends RuntimeException{

	public static enum ExceptionCode{
		INVALID_NUMBER_OF_PERSONS,
		INVALID_BASE_PRICE,
		INVALID_MATERIAL
	}

	private ExceptionCode code;
	private String message;

	MarkupCalculatorException(ExceptionCode code){
		super();
		this.message = generateMessage(code);
		this.code = code;
	}

	public String getMessage(){
		return message;
	};

	public ExceptionCode getCode(){
		return code;
	};

	private String generateMessage(ExceptionCode code){
		String msg = "";
		switch (code){
			case INVALID_NUMBER_OF_PERSONS:
				msg = "Invalid number of Persons: Allowed values between " + String.valueOf(Persons.MIN.value)
				+ " and " + String.valueOf(Persons.MAX.value);
				break;
			case INVALID_BASE_PRICE:
				msg = "Invalid Base Price: Only positive, non-zero values allowed";
				break;
			case INVALID_MATERIAL:
				msg = "Invalid Material Exception: Given material is not allowed";
				break;
			default:
				msg = "Unknown Error";
				break;
		}
		return msg;
	}
}