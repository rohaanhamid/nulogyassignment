import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import markupcalculator.*;

public class MarkupCalculatorTestRunner {
   public static void main(String[] args) {
      Result result = JUnitCore.runClasses(MarkupCalculatorTest.class);
      for (Failure failure : result.getFailures()) {
         System.out.println(failure.toString());
      }
   	  int ret = 1;
      if(result.wasSuccessful()){
      	    System.out.println("All tests completed successfully");
      	    ret = 0;
      }
      System.exit(ret);
   }
}  